//
//  Basic.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol FinishFlow {
    var finishFlow:( () -> Void )? {get set}
}

public protocol Coordinatable: class, FinishFlow {
    func start()
}

public protocol Presentable {
    /// Weak reference to view for present from coordinator
    var currentPresentor: UIViewController? { get set }
}
