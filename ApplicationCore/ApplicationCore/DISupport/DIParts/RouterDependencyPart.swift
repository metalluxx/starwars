//
//  RouterDI.swift
//  ApplicationService
//
//  Created by Metalluxx on 05/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import DITranquillity

final public class RouterDependencyPart: DIPart {
    static public func load(container: DIContainer) {
        container.register(Router.init)
            .as(Routable.self)
            .lifetime(.single)
        container.initializeSingletonObjects()
    }
}
