//
//  UNIXTimeString.swift
//  CoreArcService
//
//  Created by Metalluxx on 09/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public extension String {
    init(unixTime: Double) {
        let date = Date(timeIntervalSince1970: unixTime)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeZone = .current
        self = dateFormatter.string(from: date)
    }
}
