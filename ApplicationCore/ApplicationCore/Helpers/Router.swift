//
//  Router.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//
import UIKit

/// Default router implementation
open class Router: Routable {

    /// Main NC in hierarchy
    public let navigationController: UINavigationController = {
        var navigationController = UINavigationController()
        navigationController.view.frame = UIScreen.main.bounds
        return navigationController
    }()

    private var window: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        return window
    }()
    
    public init() {
        navigationController.setNavigationBarHidden(true, animated: false)
        window.rootViewController = navigationController
        window.backgroundColor = .black
        window.makeKeyAndVisible()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
//            self.flipDownPosition = .flipDowned
//        }
    }
    
    
    //MARK: - Flip down control

    enum FlipDownPosition: CaseIterable {
        case normal
        case flipDowned
    }
    
    var scaleFlipDown = Double(0.85)
    
    var flipDownPosition = FlipDownPosition.normal {
        willSet {
            if newValue != flipDownPosition {
                switch newValue {
                case .flipDowned:
                    let animation = CABasicAnimation(keyPath: "transform.scale")
                    animation.duration = 0.4
                    animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
                    animation.fromValue = NSNumber(floatLiteral: 1)
                    animation.toValue = NSNumber(floatLiteral: scaleFlipDown)
                    window.rootViewController?.view.layer.add(animation, forKey: nil)
                    window.rootViewController?.view.layer.transform = CATransform3DMakeScale(CGFloat(scaleFlipDown), CGFloat(scaleFlipDown), 1)
                    
                case .normal:
                    let animation = CABasicAnimation(keyPath: "transform.scale")
                    animation.duration = 0.4
                    animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
                    animation.fromValue = NSNumber(floatLiteral: scaleFlipDown)
                    animation.toValue = NSNumber(floatLiteral: 1)
                    window.rootViewController?.view.layer.add(animation, forKey: nil)
                    window.rootViewController?.view.layer.transform = CATransform3DMakeScale(1, 1, 1)
                }
            }
        }
    }

    
    //MARK: - Manage flow and transation

    /// Remove all VC from hierarcy and insert module in top
    ///
    /// - Parameter module: inserting module
    open func setRootModule(_ module: Presentable?) {
        guard let toVC = module?.currentPresentor else {return}
        guard let fromVC = navigationController.viewControllers.last else {
            navigationController.setViewControllers([toVC], animated: false)
            return
        }
        self.navigationController.setViewControllers([toVC], animated: false)
        UIWindow.transition(from: fromVC.view, to: toVC.view, duration: 0.6, options: [.transitionFlipFromLeft, .curveEaseInOut])
    }

    /// Remove all VC from hierarcy and insert module in top with animation
    ///
    /// - Parameters:
    ///   - module: inserting module
    ///   - transitionOption: transition param(array)
    open func setRootModule(_ module: Presentable?, transitionOption: UIView.AnimationOptions) {
        guard let toVC = module?.currentPresentor else {return}
        guard let fromVC = navigationController.viewControllers.last else {
            navigationController.setViewControllers([toVC], animated: false)
            return
        }
        self.navigationController.setViewControllers([toVC], animated: false)
        UIWindow.transition(from: fromVC.view, to: toVC.view, duration: 0.6, options: transitionOption)
    }

    open func push(_ module: Presentable?) {
        push(module, animated: true, completion: nil)
    }

    open func push(_ module: Presentable?, animated: Bool) {
        push(module, animated: animated, completion: nil)
    }

    open func push(_ module: Presentable?, animated: Bool, completion:( () -> Void )?) {
        guard let viewController = module?.currentPresentor else {return}
        navigationController.pushViewController(viewController, animated: animated)
        completion?()
    }

    open func popModule() {
        popModule(animated: true)
    }

    open func popModule(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }


//    func dismissModule() {
//        dismissModule(animated: true, completion: nil)
//    }
//
//    func dismissModule(animated: Bool, completion: CompletionBlock?) {
//        window.rootViewController?.dismiss(animated: animated, completion: completion)
//    }
//
//    func present(_ module: Presentable?) {
//        present(module, animated: true)
//    }
//
//    func present(_ module: Presentable?, animated: Bool) {
//        guard let viewController = module?.currentPresentor else {return}
//        window.rootViewController?.present(viewController, animated: animated, completion: {
//
//        })
//    }
}


