//
//  StringError.swift
//  ApplicationService
//
//  Created by Metalluxx on 15/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

extension String: Error {}
extension String: LocalizedError {
    public var errorDescription: String? { return self }
}
