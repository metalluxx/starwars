//
//  AnyImage.swift
//  ApplicationCore
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metallixx. All rights reserved.
//

import UIKit
import Kingfisher

public protocol AnyImage {}

extension UIImage: AnyImage {}
extension URL: AnyImage {}

public extension UIImageView {
    func set(anyImage object: AnyImage, placeholder: UIImage? = nil) {
        if let object = object as? UIImage {
            self.image = object
        }
        else if let object = object as? URL {
            self.kf.setImage(with: object, placeholder: placeholder)
        }
    }
}
