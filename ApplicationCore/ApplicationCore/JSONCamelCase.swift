//
//  JSONCamelCase.swift
//  MDW
//
//  Created by Metalluxx on 28/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import DITranquillity

public protocol JSONCamelCaseDecoder {}
public protocol JSONCamelCaseEncoder {}

public class JSONCamelCase {
    public static func initialize() {
        let container = DIContainer()
        container.register(JSONDecoder.init)
            .injection {
            $0.keyDecodingStrategy = .convertFromSnakeCase
        }
            .as(JSONDecoder.self, tag: JSONCamelCaseDecoder.self)

        container.register(JSONEncoder.init)
            .injection {
            $0.keyEncodingStrategy = .convertToSnakeCase
        }
            .as(JSONEncoder.self, tag: JSONCamelCaseEncoder.self)
    }

    public static var Decoder: JSONDecoder {
        get {
            let dec = JSONDecoder()
            dec.keyDecodingStrategy = .convertFromSnakeCase
            return dec
        }
    }
    public static var Encoder: JSONEncoder {
        get {
            let enc = JSONEncoder()
            enc.keyEncodingStrategy = .convertToSnakeCase
            return enc
        }
    }
}
