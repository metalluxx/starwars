//
//  API+RawRepres.swift
//  ApplicationService
//
//  Created by Metalluxx on 28/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public extension APIStructure
    where Self: RawRepresentable, Self.RawValue == String
{
    var path: String {
        return self.rawValue
    }
}

public extension APIStructure {
    var keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy {
        return .useDefaultKeys
    }
    
    var urlParams: [String: String?]? {
        return nil
    }
    
    var contentType: ContentType {
        return .json
    }
    
    static var sharedHeaders: [String: String]? {
        return nil
    }
}

public extension DecodableAPIStructure {
    var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy {
        return .useDefaultKeys
    }
}
