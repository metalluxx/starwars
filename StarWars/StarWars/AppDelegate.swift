//
//  AppDelegate.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application
        (_ application: UIApplication,
         didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
        -> Bool
    {
        window = UIWindow()
        window?.rootViewController = ListViewController()
        window?.makeKeyAndVisible()
        return true
    }
}

