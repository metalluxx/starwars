//
//  StarwarsAPI.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import SwiftyBeaver
import ApplicationCore

public class StarwarsGateway {
    // MARK: Debug
    deinit {
        SwiftyBeaver.debug("Deallocated \(self) \(Unmanaged.passUnretained(self).toOpaque()) ")
    }
    
    // MARK: Gateway
    static var sharedGateway = DefaultGateway<API>()
    var sharedGateway: DefaultGateway<API> {
        return StarwarsGateway.sharedGateway
    }
    
    // MARK: API
    public enum API: String, DecodableAPIStructure {
        case people, planets, films, species, vehicles, starships
        
        public var baseURL: URL {
            return URL(string: "https://swapi.co/api/")!
        }
        
        public var body: HTTPBody {
            return .none
        }
        
        public var httpMethod: HTTPMethod {
            return .post
        }
        
        public var headers: [String : String]? {
            return ["format": "json"]
        }
        
        public var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy {
            return .convertFromSnakeCase
        }
    }
    
    // MARK: Requests
    @discardableResult func people(handler: @escaping (Result<NetworkResponse<[People]>, Error>) -> Void ) -> URLSessionTask? {
        let task = sharedGateway
            .dataRequest(.people, objectType: NetworkResponse<[People]>.self)
            { (result, response) in
                handler(result)
                SwiftyBeaver.debug("task completed: \(#function) \(String(describing: result)) ")
            }
        SwiftyBeaver.debug("task created: \(#function) \(String(describing: task)) ")
        return task
    }
}
