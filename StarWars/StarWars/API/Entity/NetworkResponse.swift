//
//  NetworkResponse.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

struct NetworkResponse<ResultType: Decodable>: Decodable {
    let count: Int
    let next: String?
    let previous: String?
    let results: ResultType
}
