//
//  People.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

struct People: Decodable {
    let name: String
    let birthYear: String
    let created: String
    let edited: String
    let eyeColor: String
    let hairColor: String
    let skinColor: String
    let height: String
    let mass: String
    let homeworld: String
}
