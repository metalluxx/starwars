//
//  ListCell.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import TinyConstraints

class ListCell: UICollectionViewCell {
    // MARK: Views
    lazy var imageView = UIImageView()
    lazy var titleView = UILabel()
    
    static let ReuseID = "ListCell"
    
    // MARK: Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleView.textAlignment = .center
        
        contentView.addSubview(imageView)
        contentView.addSubview(titleView)
        
        imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.8).isActive = true
        imageView.top(to: contentView)
        imageView.left(to: contentView)
        imageView.right(to: contentView)
        
        titleView.topToBottom(of: imageView)
        titleView.left(to: contentView)
        titleView.right(to: contentView)
        titleView.bottom(to: contentView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Resizing label font
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        titleView.font = .systemFont(ofSize: titleView.frame.height - 2)
    }
}
