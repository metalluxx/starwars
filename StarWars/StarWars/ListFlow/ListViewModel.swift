//
//  ListViewModel.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import ApplicationCore
import RxCocoa
import RxSwift
import SwiftyBeaver

class ListViewModel: ReactiveViewModel {
    // MARK: Debug
    deinit {
        SwiftyBeaver.debug("Deallocated \(self) \(Unmanaged.passUnretained(self).toOpaque()) ")
    }
    
    // MARK: Typealias
    typealias Input = ListFlow.Input
    typealias Output = ListFlow.Output
    typealias Data = ListFlow.Data
    
    // MARK: Download task
    var task: URLSessionTask?
    
    // MARK: Sequence
    lazy var downloadHandlerEvent = BehaviorSubject<Void>(value: ())
    lazy var presentableData = BehaviorSubject<[Data]?>(value: nil)
    lazy var downloadedData = BehaviorSubject<Result<NetworkResponse<[People]>, Error>?>(value: nil)
    
    // MARK: ReactiveViewModel
    required init() {}
    
    func transform(input: Input, bag: DisposeBag) -> Output {
        input.vcDidAppear.bind(to: downloadHandlerEvent).disposed(by: bag)
        downloadedData.map(convertToData).bind(to: presentableData).disposed(by: bag)
        
        downloadHandlerEvent
            .share()
            .subscribe
            { [weak self] (event) in
                StarwarsGateway().sharedGateway.cancel(task: self?.task)
                self?.task = StarwarsGateway().people
                    { (result) in
                        self?.downloadedData.onNext(result)
                    }
            }
            .disposed(by: bag)
        
        return Output(
            listData: presentableData
                .map({ $0 == nil ? [Data]() : $0! })
                .asDriver(onErrorJustReturn: [])
        )
    }
}

 fileprivate extension ListViewModel {
    func convertToData(_ input: Result< NetworkResponse<[People]>, Error >?) -> [ListFlow.Data] {
        return input?.convertedData ?? []
    }
}

fileprivate extension Result where Success == NetworkResponse<[People]>, Failure: Error {
    var convertedData: [ListFlow.Data] {
        guard case let Result.success(object) = self else { return [] }
        return object.results.map { (people) -> ListFlow.Data in
            ListFlow.Data(
                text: people.name,
                image: nil
            )
        }
    }
}
