//
//  ListFlow.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import ApplicationCore
import RxSwift
import RxCocoa

enum ListFlow: ReactiveFlow {
    struct Data {
        let text: String
        let image: AnyImage?
    }
    
    struct Input {
        let vcDidAppear: Observable<Void>
    }
    struct Output {
        let listData: Driver<[Data]>
    }
}
