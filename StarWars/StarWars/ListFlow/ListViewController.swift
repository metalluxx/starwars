//
//  ListViewController.swift
//  StarWars
//
//  Created by Араик Гарибян on 30/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit
import TinyConstraints
import RxCocoa
import RxSwift
import ApplicationCore
import SwiftyBeaver

class ListViewController: UIViewController, ReactiveViewController {
    // MARK: Debug
    deinit {
        SwiftyBeaver.debug("Deallocated \(self) \(Unmanaged.passUnretained(self).toOpaque()) ")
    }
    
    // MARK: Typealias
    typealias Input = ListFlow.Input
    typealias Output = ListFlow.Output
    
    // MARK: View's
    lazy var buildedCollectionLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        return layout
    }()
    
    lazy var listView = UICollectionView(
        frame: .zero,
        collectionViewLayout: buildedCollectionLayout
    )
    
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        settingConstraints()

        bind(
            output: viewModel.transform(
                input: self.input,
                bag: bag
            )
        )
    }
    
    func configureViews() {
        view.backgroundColor = .white
        listView.backgroundColor = .white
        
        listView.register(ListCell.self, forCellWithReuseIdentifier: ListCell.ReuseID)
        listView.delegate = self
    }
    
    func settingConstraints() {
        view.addSubview(listView)        
        listView.edgesToSuperview()
    }

    // MARK: ReactiveViewController
    lazy var viewModel = ListViewModel()
    var bag = DisposeBag()
    
    // Input data in ViewModel
    var input: Input {
        return Input(
            vcDidAppear: self.rx.methodInvoked(#selector(UIViewController.viewDidAppear)).map({ _ in return () })
        )
    }
    
    // Output data in ViewModel
    func bind(output: Output) {
        output
            .listData
            .asObservable()
            .bind( to: listView.rx.items) { (view, index, data) -> UICollectionViewCell in
                guard
                    let cell = view
                        .dequeueReusableCell(
                            withReuseIdentifier: ListCell.ReuseID,
                            for: IndexPath(item: index, section: 0)
                        ) as? ListCell
                else { return .init() }
                
                cell.titleView.text = data.text
                return cell
            }
            .disposed(by: bag)
    }
    
}

extension ListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath)
        -> CGSize
    {
        return CGSize(
            width: collectionView.frame.width / 2 - 10,
            height: collectionView.frame.width / 2
        )
    }
}
